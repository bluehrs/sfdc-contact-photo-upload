@isTest
private class ContactPhotoUploadControllerTest {

	@isTest private static void testContactPhotoUploadController() {
		
		Test_DataFactory.createOrgDefaultId(true);
        Queueable_Control__c qControl = Test_DataFactory.createMainQueueable(true);

        Account a = Test_DataFactory.createAccount('Test Contact Photo', true);
        Contact c = Test_DataFactory.createContact(a.Id, 'Test', 'Task Contact Photo' , true);

        Blob bodyBlob = Blob.valueOf('Unit Test File Body');
        ContactPhotoUploadController.saveChunk(c.Id, 'Test Photo', EncodingUtil.base64Encode(bodyBlob), null);
        c = [SELECT Id, Name, Photo_Id__c FROM Contact WHERE Id = :c.Id];
        ContactPhotoUploadController.saveChunk(c.Id, 'Test Photo 2', EncodingUtil.base64Encode(bodyBlob), c.Photo_Id__c);
        ContactPhotoUploadController.getProfilePicture(c.Id);
	}
}