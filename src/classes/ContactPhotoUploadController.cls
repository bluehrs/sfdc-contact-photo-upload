public with sharing class ContactPhotoUploadController {
	
	// Gets picture on initial load of page
    @AuraEnabled
    public static List<ContentVersion> getProfilePicture(Id parentId) {
        // Attachment permissions are set in parent object (Contact)
        if (!Schema.sObjectType.Contact.isAccessible()) {
            throw new System.NoAccessException();
        }
        // Get File indicated in Photo Id field on Contact
 		Contact c = [SELECT Photo_Id__c FROM Contact WHERE Id = :parentId];
        return [SELECT Id FROM ContentVersion WHERE Id = :c.Photo_Id__c];
    }
    
    // Saves file on picture drop
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String fileId) {
        // Edit permission on parent object (Contact) is required to add attachments
        if (!Schema.sObjectType.Contact.isUpdateable()) {
            throw new System.NoAccessException();
        }
        // If File Id blank, save file and get File Id
        if (String.isBlank(fileId)) {
            fileId = saveTheFile(parentId, fileName, base64Data);
        } 
        //  Append data to existing file
        else {
            appendToFile(fileId, base64Data);
        }
        // Return File Id
        return Id.valueOf(fileId);
    }
 
    // Makes initial save of file
    public static Id saveTheFile(Id parentId, String fileName, String base64Data) {
        // Encodes data appropriately
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        // Get Contact info
        Contact c = [SELECT Name, Photo_Id__c FROM Contact WHERE Id = :parentId];
       	// Query existing Photo on Contact
        List<ContentDocumentLink> photoList = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :c.Id AND ContentDocument.LatestPublishedVersionId = :c.Photo_Id__c];
		// Insert File
        ContentVersion file = new ContentVersion(
            VersionData = EncodingUtil.base64Decode(base64Data),
            Title = c.Name + ' Photo',
            PathOnClient = '/' + fileName,
            // Upload as new version to existing Content Document if Contact Photo already exists
            ContentDocumentId = !photoList.isEmpty() ? photoList[0].ContentDocumentId : null,
            FirstPublishLocationId = photoList.isEmpty() ? c.Id : null,
            IsMajorVersion = false
        );
        insert file;
        // Update Contact Photo Id field
        c.Photo_Id__c = file.Id;
        update c;
        // Return File Id
        return file.Id;
    }
 
    // Append next chunk to File
    public static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        // Get current data
        ContentVersion cv = [SELECT VersionData FROM ContentVersion WHERE Id = :fileId];
 		// Append chunk to file and update
        cv.VersionData = EncodingUtil.base64Decode(EncodingUtil.base64Encode(cv.VersionData) + base64Data);
        update cv;
    }
}