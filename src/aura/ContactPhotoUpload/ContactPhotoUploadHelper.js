({
    readFile: function(component, helper, file) {
        // Return if empty file
        if (!file) return;
        // Return if image file
        if (!file.type.match(/(image.*)/)) {
  			return alert('Image file not supported');
		}
        // Read file
        var reader = new FileReader();
        reader.onloadend = $A.getCallback(function() {
            var fileContents = reader.result;
            // Set image to display while image uploads to Salesforce
            component.set("v.pictureSrc", fileContents);
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
            fileContents = fileContents.substring(dataStart);
            // Call helper method to start upload process
            helper.uploadProcess(component, file, fileContents);
        });
        reader.readAsDataURL(file);
	},
    uploadProcess: function(component, file, fileContents) {
        component.set("v.uploading", true);
        component.set("v.message", "Uploading...");
        // set a default size or startpostiton as 0 
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value   
        var endPosition = Math.min(fileContents.length, startPosition + 750000);
        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
    },
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        // Get current file chunk
        var getchunk = fileContents.substring(startPosition, endPosition);
        // Prepare call to save file
        var action = component.get("c.saveChunk");
        action.setParams({
            parentId: component.get("v.recordId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            fileId: attachId
        });
        // Set call action
        action.setCallback(this, function(response) {
            // If success
            if (response.getState() === "SUCCESS") {
                // Update the start position with end position
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + 750000);
                // Update progress bar
                component.set("v.progress", Math.floor((startPosition/endPosition) * 100));
                // If Start Position less than End Position, upload next chunk
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, response.getReturnValue());
                }
                // Otherwise finished so update message and turn off progress bar
                else {
                    component.set("v.message", "Image uploaded");
                    component.set("v.uploading", false);
                }       
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
    }
})