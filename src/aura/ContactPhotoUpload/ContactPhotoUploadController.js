({  
    // Load current profile picture
    onInit: function(component) {
        var action = component.get("c.getProfilePicture"); 
        action.setParams({
            parentId: component.get("v.recordId"),
        });
        action.setCallback(this, function(a) {
            var files = a.getReturnValue();
            if (files && files.length > 0) {
	            component.set("v.pictureSrc", "/sfc/servlet.shepherd/version/download/" + files[0].Id);
            }
        });
        $A.enqueueAction(action);
    },
    
    onDragOver: function(component, event) {
        event.preventDefault();
    },

    onDrop: function(component, event, helper) {
		event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        if (files.length > 1) {
            return alert("You can only upload one Contact Photo.");
        }
        if (files[0].size > 4500000) {
            return alert('File size cannot exceed 4.5MB.\n' + 'Selected file size: ' + files[0].size);
        }
        helper.readFile(component, helper, files[0]);
	}
    
})