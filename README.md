# SFDC Contact Photo Upload

This project uses a Lightning Component to drag-and-drop upload and display photos (stored as Chatter Files aka Content Versions linked to the Contact via a Content Document Link).

**Notes:**

- The controller finds and stores the Contact Photo by writing to a Photo_Id__c text field on Contact.

- If a Contact Photo already exists on the Contact, the controller will add a new ContentVersion to the existing ContentDocument.

- You can use the Lightning Component in Lightning Experience or you can use the Lightning Component via Lightning Out with the ContactPhotoUpload Visualforce Page.

- I took a good amount of this code from other projects around the web. I can't find them right now, but I'll add those links if I find them again.